/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author mc-so
 */
public class TestTableForOX {

    public TestTableForOX() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testRow1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0, false);
        table.setRowCol(0, 1, false);
        table.setRowCol(0, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testRow2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(1, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testRow3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0, false);
        table.setRowCol(2, 1, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testRow1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0, false);
        table.setRowCol(0, 1, false);
        table.setRowCol(0, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testRow2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(1, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(1, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testRow3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(2, 0, false);
        table.setRowCol(2, 1, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testCol1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0, false);
        table.setRowCol(1, 0, false);
        table.setRowCol(2, 0, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testCol2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 1, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(2, 1, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testCol3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2, false);
        table.setRowCol(1, 2, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testCol1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0, false);
        table.setRowCol(1, 0, false);
        table.setRowCol(2, 0, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testCol2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 1, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(2, 1, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testCol3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 2, false);
        table.setRowCol(1, 2, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testSlashByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testBackSlashByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(0, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(x.getName(), table.getWinner());
    }

    public void testSlashByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }

    public void testBackSlashByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(2, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(0, 2, false);
        table.checkWinner();
        assertEquals(true, table.isFinish);
        assertEquals(o.getName(), table.getWinner());
    }
    
        public void testTie() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0, false);
        table.setRowCol(0, 2, false);
        table.setRowCol(1, 2, false);
        table.setRowCol(2, 0, false);
        table.setRowCol(2, 1, false);
        table.switchPlayer();
        table.setRowCol(0, 1, false);
        table.setRowCol(1, 0, false);
        table.setRowCol(1, 1, false);
        table.setRowCol(2, 2, false);
        table.checkWinner();
        assertEquals(false, table.isFinish);
        assertEquals('-', table.getWinner());
    }
}
