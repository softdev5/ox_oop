/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mc-so
 */
public class Table {

    private char table[][]
            = {{'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastRow, lastCol;
    public boolean isFinish = false;
    public Player winner = new Player('-');
    private int round = 1;

    public Table(Player x, Player o) {
        this.playerO = o;
        this.playerX = x;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col, boolean pass) {
        if (row < 3 && row >= 0 && col < 3 && col >= 0) {
            pass = checkTable(row, col);
        } else {
            System.out.println("Your input is out of range,"
                    + " pleas input again!");
        }
        return pass;
    }

    public boolean checkTable(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            return true;
        } else {
            System.out.println("This spot already used, please input again!");
            return false;

        }

    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
        round++;

    }

    public void checkRow() {
        for (int row = 0; row < table.length; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;
    }

    public void checkCol() {
        for (int col = 0; col < table[lastRow].length; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;
    }

    public void checkBackSlash() {
        for (int col = 0; col < table[lastRow].length; col++) {
            if (table[col][col] != currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;
    }

    public void checkSlash() {
        int count = 2;
        for (int row = 0; row < table.length; row++) {
            if (table[row][count] != currentPlayer.getName()) {
                return;
            }
            count--;
        }
        isFinish = true;
        winner = currentPlayer;
    }

    public boolean checkWinner() {
        checkTie();
        checkRow();
        checkCol();
        checkSlash();
        checkBackSlash();
        return isFinish;
    }

    public char getWinner() {
        return winner.getName();
    }

    public void checkTie() {
        if (round == 9) {
            isFinish = true;
            winner = new Player('-');
        }
    }
}
