
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mc-so
 */
public class Game {

    private Scanner scanner = new Scanner(System.in);
    private Player playerX;
    private Player playerO;
    private Player turn;
    private Table table;
    private int row, col;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        boolean pass = false;
        while (pass == false) {
            System.out.println("Please input Row Col:");
            row = scanner.nextInt() - 1;
            col = scanner.nextInt() - 1;
            pass = table.setRowCol(row, col, pass);
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn....");
    }

    public void showBye() {
        System.out.println("Bye Bye....");
    }

    public void showResult() {
        showTable();
        if (table.getWinner() == '-') {
            System.out.println("Tie Game!!");
        } else {
            System.out.println("Player " + table.getWinner() + " Win!!");
        }
    }

    public void newGame() {
        System.out.println("Do you want to play again? Y/N");
        if (scanner.next().charAt(0) == 'Y') {
            Game game = new Game();
            game.run();
        } else {
            showBye();
        }
    }

    public void run() {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            input();
            if (table.checkWinner() == true) {
                break;
            }
            table.switchPlayer();
        }
        showResult();
        newGame();

    }

}
